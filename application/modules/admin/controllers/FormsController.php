<?php
class Admin_FormsController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('forms');
		Zend_Loader::loadClass('fields');
		Zend_Loader::loadClass('pages');

	}

	function indexAction() {

		$obj = new Forms();
		$this->view->results = $obj->fetchAll();

	}


	// add new record
	function addAction() {

		$form = new Forms();
		$fields = new Fields();
		$pages = new Pages();
		$request = $this->getRequest();

		$this->view->pages = $pages->fetchAll();

		if($request->isPost()) {

			$post = $request->getPost();

			$form_success_msg = '';
			if($post['message_text']) {
				$form_success_msg = $post['message_text'];
			}
			if($post['message_page']) {
				$form_success_msg = $post['message_page'];
			}
			if($post['message_redirect']) {
				$form_success_msg = $post['message_redirect'];
			}

			$post = array_merge($request->getPost(),array('message'=>$form_success_msg));
			$formid = $form->saveForm($post);

			// add fields
			$data = '';
			$i = 0;
			foreach($post['field_type'] as $key => $val) {
				$data = array(
					'formId'		=> $formid,
					'type'			=> $post['field_type'][$key],
					'name'			=> $post['field_name'][$key],
					'description'	=> $post['field_description'][$key],
					'options'		=> @($post['field_options'][$key])?serialize($post['field_options'][$key]):'',
					'required'		=> $post['field_required'][$key],
					'rank'			=> $i++
				);
				$fields->saveField($data);
			}

			$this->_redirect("/admin/forms");

		}

	}


	// edit new record
	function editAction() {

		$form = new Forms();
		$fields = new Fields();
		$pages = new Pages();
		$request = $this->getRequest();

		$id = $request->getParam('id');

		$this->view->row = $form->getForm($id);
		$this->view->fields = $fields->getFormFields($id);
		$this->view->pages = $pages->fetchAll();

		if($request->isPost()) {

			$post = $request->getPost();

			$form_success_msg = '';
			if($post['message_text']) {
				$form_success_msg = $post['message_text'];
			}
			if($post['message_page']) {
				$form_success_msg = $post['message_page'];
			}
			if($post['message_redirect']) {
				$form_success_msg = $post['message_redirect'];
			}

			$post = array_merge($request->getPost(),array('message'=>$form_success_msg));
			$formid = $form->saveForm($post);

			$fields->deleteFieldsByFormId($id);

			// add fields
			$data = '';
			$i = 0;
			foreach($post['field_type'] as $key => $val) {
				$data = array(
					'formId'		=> $formid,
					'type'			=> $post['field_type'][$key],
					'name'			=> $post['field_name'][$key],
					'description'	=> $post['field_description'][$key],
					'options'		=> @($post['field_options'][$key])?serialize($post['field_options'][$key]):'',
					'required'		=> $post['field_required'][$key],
					'rank'			=> $i++
				);
				$fields->saveField($data);
			}

			$this->_redirect("/admin/forms");

		}

	}


	// delete new record
	function deleteAction() {

		$forms = new Forms();
		$fields = new Fields();
		$id = $this->_request->getParam('id');

		if($id) {
			$forms->delForm($id);
			$fields->deleteFieldsByFormId($id);
			$this->_redirect("/admin/forms");
		} else {
			$this->_redirect("/admin/forms");
		}

	}

}