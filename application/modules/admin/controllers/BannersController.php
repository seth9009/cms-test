<?php
class Admin_BannersController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('banners');
		Zend_Loader::loadClass('upload');

	}

	function indexAction() {

		$obj = new Banners();
		$this->view->results = $obj->fetchAll();

	}


	// add new record
	function addAction() {

		$page = new Banners();

		$request = $this->getRequest();
		if($request->isPost()) {

			$image = '';
			$handle = new Upload($_FILES['image']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 250;
				$handle->image_y = 150;
				$handle->Process("./upload/banners/");
				$image = $handle->file_dst_name;
			}
 			$handle->clean();

			$post = array_merge($request->getPost(),array('image'=>$image));
			$page->save($post);
			$this->_redirect("/admin/banners");

		}

	}


	// edit new record
	function editAction() {

		$obj = new Banners();
		$request = $this->getRequest();

		$this->view->row = $obj->getImageById($request->getParam('id'));

		if($request->isPost()) {

			$image = '';
			$handle = new Upload($_FILES['image']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 250;
				$handle->image_y = 150;
				$handle->Process("./upload/banners/");
				$image = $handle->file_dst_name;
			}
 			$handle->clean();

			$post = array_merge($request->getPost(),array('image'=>$image));
			$obj->save($post);
			$this->_redirect("/admin/banners");

		}

	}


	// delete new record
	function deleteAction() {

		$obj = new Banners();
		$id = $this->_request->getParam('id');

		if($id) {
			$obj->del($id);
			$this->_redirect("/admin/banners");
		} else {
			$this->_redirect("/admin/banners");
		}

	}


}