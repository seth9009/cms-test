<?php
class Admin_AdminusersController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('admin');

	}

	function indexAction() {

		$obj = new Admin();
		$this->view->results = $obj->getUsers();

	}


	// add new record
	function addAction() {

		$obj = new Admin();
		$request = $this->getRequest();

		if($request->isPost()) {
			$obj->saveUser($request->getPost());
			$this->_redirect("/admin/adminusers");
		}

	}

	// edit new record
	function editAction() {

		$obj = new Admin();
		$request = $this->getRequest();

		$row = $obj->getUser($request->getParam('id'));
		$this->view->row = $row;

		if($request->isPost()) {
			$obj->saveUser($request->getPost());
			$this->_redirect("/admin/adminusers");
		}

	}

	function deleteAction() {

		$obj = new Admin();
		$request = $this->getRequest();

		$obj->delUser($request->getParam('id'));

		$this->_redirect("/admin/adminusers");

	}

}