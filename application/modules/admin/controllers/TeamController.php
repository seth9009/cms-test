<?php
class Admin_TeamController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('teams');
		Zend_Loader::loadClass('upload');

	}

	function indexAction() {

		$obj = new Teams();
		$this->view->results = $obj->fetchAll();

	}


	// add new record
	function addAction() {

		$page = new Teams();

		$request = $this->getRequest();
		if($request->isPost()) {

			$pic = '';
			$handle = new Upload($_FILES['pic']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 200;
				$handle->image_y = 250;
				$handle->Process("./upload/team/");
				$pic = $handle->file_dst_name;
			}
 			$handle->clean();

			$main_pic = '';
			$handle = new Upload($_FILES['main_pic']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 500;
				$handle->image_y = 300;
				$handle->Process("./upload/team/");
				$main_pic = $handle->file_dst_name;
			}
 			$handle->clean();

			$post = array_merge($request->getPost(),array('pic'=>$pic,'main_pic'=>$main_pic));
			$page->saveMember($post);
			$this->_redirect("/admin/team");

		}

	}


	// edit new record
	function editAction() {

		$obj = new Teams();
		$request = $this->getRequest();

		$this->view->row = $obj->getMember($request->getParam('id'));

		if($request->isPost()) {

			$pic = '';
			$handle = new Upload($_FILES['pic']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 200;
				$handle->image_y = 250;
				$handle->Process("./upload/team/");
				$pic = $handle->file_dst_name;
			}
 			$handle->clean();

			$main_pic = '';
			$handle = new Upload($_FILES['main_pic']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 500;
				$handle->image_y = 300;
				$handle->Process("./upload/team/");
				$main_pic = $handle->file_dst_name;
			}
 			$handle->clean();

			$post = array_merge($request->getPost(),array('pic'=>$pic, 'main_pic'=>$main_pic));
			$obj->saveMember($post);
			$this->_redirect("/admin/team");

		}

	}


	// delete new record
	function deleteAction() {

		$obj = new Teams();
		$id = $this->_request->getParam('id');

		if($id) {
			$obj->delMember($id);
			$this->_redirect("/admin/team");
		} else {
			$this->_redirect("/admin/team");
		}

	}


}