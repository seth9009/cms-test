<?php
class Admin_MenusController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('menus');
		Zend_Loader::loadClass('pages');

	}

	function indexAction() {

		$pageobj = new Pages();
		$menuobj = new Menus();

		$showMenuBuild = false;
		$site = $this->_request->getParam('site');
		if($this->_request->isPost()) {
			$site = $this->_request->getPost('site');
		}
		if($site) {
			$this->view->pages = $pageobj->getPagesBySite($site);
			$showMenuBuild = true;
			$this->view->mainmenu = $menuobj->makeAdminMenu(0,0,$site);
			$this->view->showMenuBuild = $showMenuBuild;
		}
		$this->view->site = $site;

		if($this->_request->isPost()) {

			$saveBtn = $this->_request->getPost('save_menu');
			if($saveBtn) {
				$menuobj->saveMenu($this->_request->getPost());
				$this->_redirect("/admin/menus/index/site/".$site);
			}

		}

	}

	function getmenuidAction() {

		$minc = new Zend_Session_Namespace('menuincreq');
 		if(isset($minc->reqs)) {
			$minc->reqs++;
		} else {
			$minc->reqs = 1;
		}

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$menuobj = new Menus();
		$maxid = $menuobj->getMaxId();

		$mid = $maxid + $minc->reqs;
		$id = $this->_request->getParam('id');
		$name = $this->_request->getParam('name');
		$newname = str_replace(" ","_",$name);

		echo '<li id="menu-item-'.$newname.'['.$id.']-'.$mid.'" class="item-'.$mid.'"><dl class="menu-item-bar"><dt class="menu-item-handle"><span class="item-title">'.$name.'</span><span class="item-controls"><span class="item-type"><a class="remove" id="item-'.$mid.'">Delete</a></span></span></dt></dl>';

	}

}