<?php

class Admin_CommentsController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('comments');
		Zend_Loader::loadClass('Zend_Filter_StripTags');
		Zend_Loader::loadClass('Zend_Paginator');

		$this->_helper->layout->setLayout('admin');

	}

	function indexAction() {

		$obj = new Comments();
		$id = $this->_request->getParam('id');
		$this->view->id = $id;

		if($id) {
			$result = $obj->getCommentsByPostId($id);
		} else {
			$result = $obj->fetchAll();
		}
		$this->view->results = $result;

	}

	// update
	function updateAction() {

		$obj = new Comments();
		$cid = $this->_request->getParam('cid');
		$bid = $this->_request->getParam('id');
		$status = $this->_request->getParam('status');

		if($cid) {
			$obj->updateComment($cid,$status);
		}
		if($bid) {
			$this->_redirect("/admin/comments/index/id/".$bid);
		} else {
			$this->_redirect("/admin/comments");
		}

	}

	// delete record
	function deleteAction() {

		$obj = new Comments();
		$id = $this->_request->getParam('id');

		if($id) {
			$obj->delComment($id);
		}
		$this->_redirect("/admin/comments");

	}


}