<?php
class Admin_SliderController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('sliders');
		Zend_Loader::loadClass('upload');

	}

	function indexAction() {

		$obj = new Sliders();
		$this->view->results = $obj->fetchAll();

	}


	// add new record
	function addAction() {

		$page = new Sliders();

		$request = $this->getRequest();
		if($request->isPost()) {

			$image = '';
			$handle = new Upload($_FILES['image']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 840;
				$handle->image_y = 415;
				$handle->Process("./upload/sliders/");
				$image = $handle->file_dst_name;
			}
 			$handle->clean();

			$post = array_merge($request->getPost(),array('image'=>$image));
			$page->save($post);
			$this->_redirect("/admin/slider");

		}

	}


	// edit new record
	function editAction() {

		$obj = new Sliders();
		$request = $this->getRequest();

		$this->view->row = $obj->getImageById($request->getParam('id'));

		if($request->isPost()) {

			$image = '';
			$handle = new Upload($_FILES['image']);
			if($handle->uploaded) {
				$handle->file_new_name_body = time();
				$handle->image_resize = true;
				$handle->image_ratio = true;
				$handle->image_ratio_crop = true;
				$handle->image_x = 840;
				$handle->image_y = 415;
				$handle->Process("./upload/sliders/");
				$image = $handle->file_dst_name;
			}
 			$handle->clean();

			$post = array_merge($request->getPost(),array('image'=>$image));
			$obj->save($post);
			$this->_redirect("/admin/slider");

		}

	}


	// delete new record
	function deleteAction() {

		$obj = new Sliders();
		$id = $this->_request->getParam('id');

		if($id) {
			$obj->del($id);
			$this->_redirect("/admin/slider");
		} else {
			$this->_redirect("/admin/slider");
		}

	}


}