<?php
class Admin_PagesController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('pages');
		Zend_Loader::loadClass('menus');
		Zend_Loader::loadClass('sitemap');

	}

	function indexAction() {

		$pg = new Pages();
		$this->view->results = $pg->fetchAll();

	}


	// add new record
	function addAction() {

		$page = new Pages();
		$sitemap = new Sitemap();

		$request = $this->getRequest();
		if ($request->isPost()) {
			$page->savePage($this->_request->getPost());
			$sitemap->generate();
			$this->_redirect("/admin/pages");
		}

	}


	// edit new record
	function editAction() {

		$page = new Pages();
		$mobj = new Menus();
		$request = $this->getRequest();

		$this->view->page = $page->getPageById($this->_request->getParam('id'));

		if($request->isPost()) {

			$post = $this->_request->getPost();

			$page->savePage($post);

			if($post['status']=="0") {
				$mobj->delMenuByPath($post['id'],$post['site']);
			}

			$this->_redirect("/admin/pages");

		}

	}


	// delete new record
	function deleteAction() {

		$page = new Pages();
		$mobj = new Menus();
		$sitemap = new Sitemap();
		$id = $this->_request->getParam('id');

		if($id) {

			$row = $page->getPageById($id);
			$mobj->delMenuByPath($id,$row->site);

			$page->delPage($id);

			$sitemap->generate();
			$this->_redirect("/admin/pages");

		} else {

			$this->_redirect("/admin/pages");

		}

	}


}