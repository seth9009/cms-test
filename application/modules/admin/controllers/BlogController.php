<?php

class Admin_BlogController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('blogs');
		Zend_Loader::loadClass('sitemap');
		Zend_Loader::loadClass('Zend_Filter_StripTags');
		Zend_Loader::loadClass('Zend_Paginator');

		$this->_helper->layout->setLayout('admin');

	}

	function indexAction() {

		$obj = new Blogs();
		$result = $obj->fetchAll();
		$this->view->results = $result;

	}


	// add new record
	function addAction() {

		$obj = new Blogs();
		$sitemap = new Sitemap();

		$request = $this->getRequest();
		if ($request->isPost()) {
			$obj->savePost($request->getPost());
			$sitemap->generate();
			$this->_redirect("/admin/blog");
		}

	}


	// edit new record
	function editAction() {

		$obj = new Blogs();
		$request = $this->getRequest();
		
		$this->view->post = $obj->getPostById($request->getParam('id'));

		if($request->isPost()) {
			$obj->savePost($request->getPost());
			$this->_redirect("/admin/blog");
		}

	}


	// delete new record
	function deleteAction() {

		$obj = new Blogs();
		$sitemap = new Sitemap();
		$id = $this->_request->getParam('id');

		if($id) {
			$obj->delPost($id);
			$sitemap->generate();
		}
		$this->_redirect("/admin/blog");

	}


}