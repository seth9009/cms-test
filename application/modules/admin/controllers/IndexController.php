<?php

class Admin_IndexController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('Zend_Filter_StripTags');
		Zend_Loader::loadClass('Zend_Validate');
		Zend_Loader::loadClass('Zend_Form');
		Zend_Loader::loadClass('Zend_Auth_Adapter_DbTable');

	}

	function indexAction() {
		$this->_redirect('/admin/pages');
	}


	function loginAction() {

		$auth = Zend_Auth::getInstance();
		if ($auth->hasIdentity()) {
			$this->_redirect('/admin');
		}

		$this->_helper->layout->setLayout('login');

		$this->view->err_msg = '';

		if ($this->_request->isPost()) {

			$f = new Zend_Filter_StripTags();

			$user = $f->filter($this->_request->getPost('user'));
			$pass = $f->filter($this->_request->getPost('pass'));

			if(empty($user) || empty($pass)) {

				$this->view->err_msg = "Please enter the username and password.";

			} else {

				// setup Zend_Auth adapter for a database table
				$db = Zend_Registry::get('db');
				$authAdapter = new Zend_Auth_Adapter_DbTable($db);
				$authAdapter->setTableName('admin');
				$authAdapter->setIdentityColumn('user');
				$authAdapter->setCredentialColumn('pass');

				// Set the input credential values to authenticate against
				$authAdapter->setIdentity($user);
				$authAdapter->setCredential(md5($pass));

				// do the authentication
				$auth = Zend_Auth::getInstance();
				$result = $auth->authenticate($authAdapter);
				if ($result->isValid()) {

					$data = $authAdapter->getResultRowObject(null, 'pass');
					$auth->getStorage()->write($data);

					$this->_redirect('/admin/pages');

				} else {

					// failure: clear database row from session
					$this->view->err_msg = "Wrong username or password.";

				}
			}
		}

	}


	// user logout
	public function logoutAction() {

		Zend_Auth::getInstance()->clearIdentity();
		Zend_Session::destroy(true);
		$this->_redirect('/admin');
	
	}


}