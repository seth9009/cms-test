<?php

class Zend_View_Helper_FriendlyUrl {

	public function FriendlyUrl($name) {

		$name = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $name);
		$name = strtolower(trim($name));
		$name = preg_replace("/[\/_|+ -]+/", '-', $name);

		return $name;

	}

}