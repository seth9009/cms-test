<?php

class Zend_View_Helper_AdminMenu {

	private $_view;
	public function setView($view) {
		$this->_view = $view;
	}

	public function AdminMenu() {

		$topmenu = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
		if($topmenu == "index") {
			$topmenu = "";
		}

		$menus = array();
		$menus['Pages'] = "pages";
//		$menus['Blog'] = "blog";
		$menus['Forms'] = "forms";
		$menus['Menus'] = "menus";
		$menus['Team'] = "team";
		$menus['Home Slider'] = "slider";
		$menus['4 Banners'] = "banners";
		$menus['Admin Users'] = "adminusers";

		$html = '<ul id="dashboard-menu">';
		foreach($menus as $name => $url) {
			$ico = strtolower(str_replace(" ","-",$name));
			if($topmenu == $url) {
				$html .= '<li><a href="'.$this->_view->LinkTo('/admin/'.$url).'">';
				$html .= '<i class="icon-'.$ico.'"></i><span>'.$name.'</span></a>';
				$html .= '<div class="pointer"><div class="arrow"></div><div class="arrow_border"></div></div>';
				$html .= '</li>';
			} else {
				$html .= '<li><a href="'.$this->_view->LinkTo('/admin/'.$url).'">';
				$html .= '<i class="icon-'.$ico.'"></i><span>'.$name.'</span></a>';
				$html .= '</li>';
			}
		}
		$html .= "</ul>";

		return $html;

	}

}