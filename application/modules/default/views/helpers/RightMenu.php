<?php
Zend_Loader::loadClass('menus');
Zend_Loader::loadClass('pages');

class Zend_View_Helper_RightMenu {

	private $_view;

	public function setView($view) {
		$this->_view = $view;
	}

	public function RightMenu() {

		$mobj = new Menus();
		$pobj = new Pages();

		$request = Zend_Controller_Front::getInstance()->getRequest();

		$permalink = $request->getParam('permalink');
		$parentPage = $pobj->getPageByPerma($permalink);
		$pageid = $parentPage['id'];
		$pid = $mobj->getIdByPath($pageid);

		if($pid) {

			$pid = $this->GetMainPage($pid);

			$menus = $mobj->getMenusByParentId($pid->id);

			if(count($menus) > 0) {

				$showmenu = '<div class="title">'.$pid['name'].'</div>';
				$showmenu .= '<ul>';
				foreach($menus as $menu) {

					if(is_numeric($menu['path'])) {
						$page = $pobj->getPageById($menu['path']);
						$path = $this->_view->LinkTo('page/'.$page['permalink']);
					} else {
						$path = $menu['path'];
					}

					$active = '';
					if($permalink==$page['permalink']) {
						$active = 'active';
					}

					$showmenu .= '<li><a class="'.$active.'" href="'.$path.'">'.$menu['name'].'</a></li>';

				}
				$showmenu .= '</ul>';

				return $showmenu;

			}

		}

		return false;

	}

	private function GetMainPage($pid) {

		$mobj = new Menus();

		if($pid->parent_id > 0) {
			$pid = $mobj->getMenuById($pid->parent_id);
			$this->GetMainPage($pid);
		}

		return $pid;

	}

}