<?php

class Zend_View_Helper_DomainName {

	private $_view;

	public function setView($view) {
		$this->_view = $view;
	}

	public function DomainName() {

		$html = 'SABETHA';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'SABETHA';
		}
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'SENECA';
		}
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'NEBRASKA';
		}
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'HUMBOLDT';
		}

		return $html;

	}

}
