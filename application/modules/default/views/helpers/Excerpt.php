<?php

class Zend_View_Helper_Excerpt {

	public function Excerpt($string,$len) {

		$string = strip_tags($string);

		$parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
		$parts_count = count($parts);

		$length = 0;
		$last_part = 0;
		for(; $last_part < $parts_count; ++$last_part) {
			$length += strlen($parts[$last_part]);
			if($length > $len) {
				break;
			}
		}
		return implode(array_slice($parts, 0, $last_part));

	}

}