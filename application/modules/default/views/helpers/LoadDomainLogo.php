<?php

class Zend_View_Helper_LoadDomainLogo {

	public function LoadDomainLogo() {

		$html = '<img src="/assets/images/logo-sabetha.png" alt="Sabetha" />';

		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '<img src="/assets/images/logo-sabetha.png" alt="Northridge Sabetha" />';
		}

		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '<img src="/assets/images/logo-seneca.png" alt="Sabetha" />';
		}

		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '<img src="/assets/images/logo-nebraska.png" alt="Northridge Nebraska" />';
		}

		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '<img src="/assets/images/logo-humboldt.png" alt="Northridge Humboldt" />';
		}

		return $html;

	}

}
