<?php

class Zend_View_Helper_FooterMenu {

	private $_view;

	public function setView($view) {
		$this->_view = $view;
	}

	public function FooterMenu() {

		Zend_Loader::loadClass('menus');
		Zend_Loader::loadClass('pages');
		$mobj = new Menus();
		$pobj = new Pages();

		$menus = $mobj->getMenusByParentId(0,'footer');

		$showmenu = '<ul>';
		foreach($menus as $menu) {

			if(is_numeric($menu['path'])) {
				$page = $pobj->getPageById($menu['path']);
				$path = $this->_view->LinkTo('page/'.$page['permalink']);
			} else {
				$path = $menu['path'];
			}

			$showmenu .= '<li><a href="'.$path.'">'.$menu['name'].'</a></li>';

		}
		$showmenu .= '</ul>';

		return $showmenu;

	}

}