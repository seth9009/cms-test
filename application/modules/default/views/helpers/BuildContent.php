<?php

class Zend_View_Helper_BuildContent {

	public function BuildContent($cont,$data) {

		preg_match('/\[(.*)\]/iU',$cont,$match);
		if(count($match) > 0) {

			if(strpos($match[1],'id=')!==false) {
				preg_match('/\[(.*)\ id\=(.+)\]/iU',$cont,$inc);
			} elseif(strpos($match[1],':')!==false) {
				preg_match('/\[(.*)\:(.*)\]/iU',$cont,$inc);
			} else {
				preg_match('/\[(.*)\]/iU',$cont,$inc);
			}

			return $this->AddModule($cont,$inc,$data);

		}

		return $cont;

	}

	public function AddModule($content,$inc,$data = false) {

        $modules = array('form','team');

        if(in_array($inc[1],$modules)) {

        	$moduleData = '';
            switch ($inc[1]) {
                case 'form':
					Zend_Loader::loadClass('forms');
					Zend_Loader::loadClass('fields');
					$form = new Forms();
					$fields = new Fields();
					$moduleData = $form->BuildForm($inc[2],$fields,$data);
                    break;
                case 'team':
					Zend_Loader::loadClass('teams');
					$team = new Teams();
					$moduleData = $team->BuildTeam();
                    break;
			}

            $cont = str_replace($inc[0], $moduleData, $content);
            return $cont;

        }

		return $content;

	}

}