<?php

class Zend_View_Helper_LoadDomainEmail {

	public function LoadDomainEmail() {

		$html = 'info@northridgesabetha.com';

		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'info@northridgesabetha.com';
		}

		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'info@northridgeseneca.com';
		}

		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'info@northridgenebraska.com';
		}

		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'info@northridgehumboldt.com';
		}

		return $html;

	}

}
