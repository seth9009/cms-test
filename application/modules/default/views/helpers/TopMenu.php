<?php

Zend_Loader::loadClass('pages');
Zend_Loader::loadClass('menus');

class Zend_View_Helper_TopMenu {

	private $_view;

	public function setView($view) {
		$this->_view = $view;
	}

	public function TopMenu() {

		$mobj = new Menus();

		$menus = $mobj->getMenuBySite();

		$showmenu = '<ul class="nav navbar-nav">';
		$showmenu .= $this->buildMenu($menus->toArray());
		$showmenu .= '</ul>';

		return $showmenu;

	}

	function buildMenu($array,$parent_id = 0,$parents = array()) {

		$pobj = new Pages();

        if($parent_id==0) {
            foreach ($array as $element) {
                if (($element['parent_id'] != 0) && !in_array($element['parent_id'],$parents)) {
                    $parents[] = $element['parent_id'];
                }
            }
        }

        $menu_html = '';
        foreach($array as $element) {
            if($element['parent_id']==$parent_id) {

				if(is_numeric($element['path'])) {
					$page = $pobj->getPageById($element['path']);
					$path = $this->_view->LinkTo('page/'.$page['permalink']);
				} else {
					$path = $element['path'];
				}

                if(in_array($element['id'],$parents)) {
                    $menu_html .= '<li class="has-dropdown">';
                } else {
                    $menu_html .= '<li>';
                }
				$menu_html .= '<a href="'.$path.'">'.$element['name'].'</a>';
                if(in_array($element['id'],$parents)) {
                    $menu_html .= '<ul class="dropdown">';
                    $menu_html .= $this->buildMenu($array, $element['id'], $parents);
                    $menu_html .= '</ul>';
                }
                $menu_html .= '</li>';

            }
        }
        return $menu_html;

    }

}