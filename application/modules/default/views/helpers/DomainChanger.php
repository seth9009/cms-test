<?php

class Zend_View_Helper_DomainChanger {

	private $_view;

	public function setView($view) {
		$this->_view = $view;
	}

	public function DomainChanger() {

		$html = '';

		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$html .= '<a class="active" href="http://northridgesabetha.com/page">Sabetha</a>';
		} else {
			$html .= '<a href="http://northridgesabetha.com/page">Sabetha</a>';
		}

		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$html .= '<a class="active" href="http://northridgeseneca.com/page">Seneca</a>';
		} else {
			$html .= '<a href="http://northridgeseneca.com/page">Seneca</a>';
		}

		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$html .= '<a class="active" href="http://northridgenebraska.com/page">Nebraska</a>';
		} else {
			$html .= '<a href="http://northridgenebraska.com/page">Nebraska</a>';
		}

		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$html .= '<a class="active" href="http://northridgehumboldt.com/page">Humboldt</a>';
		} else {
			$html .= '<a href="http://northridgehumboldt.com/page">Humboldt</a>';
		}

		return $html;

	}

}
