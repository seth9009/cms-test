<?php

class Zend_View_Helper_LoadDomainAdr {

	public function LoadDomainAdr() {

		$html = '(785) 284-3060 <span class="desp">|</span> 316 Lincoln Street, Sabetha, KS 66534';

		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '(785) 284-3060 <span class="desp">|</span> 316 Lincoln Street, Sabetha, KS 66534';
		}

		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '(785) 284-3060 <span class="desp">|</span> 601 Castle Street, Seneca, KS 66538';
		}

		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '(402) 245-2167 <span class="desp">|</span> 1820 Morton Street, Falls City, NE 68355';
		}

		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = '(402) 245-2167 <span class="desp">|</span> 1820 Morton Street, Falls City, NE 68355';
		}

		return $html;

	}

}
