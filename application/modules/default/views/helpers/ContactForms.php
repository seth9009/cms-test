<?php

Zend_Loader::loadClass('reports');
Zend_Loader::loadClass('subscribers');

class Zend_View_Helper_ContactForms {

	public function ContactForms($cont,$data=false) {

		$robj = new Reports();
		$sobj = new Subscribers();

		$sw = 0;

		$pubKey = "6LdD9bsSAAAAAFof9zC7quo3MJZCM-ZWmylEoFc-";
		$privKey = "6LdD9bsSAAAAAB2iXN9BZ5wGrjsiP6ZYDCJlQQsg";

		$options = array('theme' => 'clean');
		$recaptcha = new Zend_Service_ReCaptcha($pubKey, $privKey, null, $options);

        if($data) {

			if(empty($data['email'])) {
				$err_txt = "Please fill in all fields<br>";
				$sw = 1; }
			if(!preg_match('/^[-!#$%&\'*+\\.\/0-9=?A-Z^_`{|}~]+@([-0-9A-Z]+\.)+([0-9A-Z]){2,4}$/i', $_POST['email'])) {
				$sw = 1;
				$err_email = "Please provide a valid email address<br>"; }

			$result = $recaptcha->verify($data['recaptcha_challenge_field'],$data['recaptcha_response_field']);

			if(($sw == 0) && $result->isValid()) {

				$row = $sobj->getByEmail($data['email']);
				if(@$row->id) {
					$sid = $row->id;
				} else {
					$sdata = array('org'		=> @$data['org'],
								   'title'		=> @$data['title'],
								   'fname'		=> @$data['fname'],
								   'lname'		=> @$data['lname'],
								   'adr'		=> @$data['adr'],
								   'city'		=> @$data['city'],
								   'state'		=> @$data['state'],
								   'zip'		=> @$data['zip'],
								   'phone'		=> @$data['phone'],
								   'email'		=> @$data['email'],
								   'newsletter' => @$data['newsletter'],
								   'comments'	=> @$data['comments']);
					$sid = $sobj->saveData($sdata);
				}

				if($cont['contact']=="report") {
					$rowrep = $robj->getById($data['report']);
					$repreq = array('sid'=>$sid,'repid'=>$data['report'],'date'=>time());
//					$robj->saveRequest($repreq);
//					$db->query("INSERT INTO `report_requests` (`subs_id`, `report_id`, `date`) VALUES ('".$subsid."', '".$_POST['rept']."', '".time()."')");
				}

				if(@$data['newsletter']=="" || @$data['newsletter']=="no") {
					$newsletter = "no";
				} else {
					$newsletter = "yes";
				}

				$msg = "Sender's information:<br />";
				$msg .= "------------------------------------------------------<br />";
				$msg .= "<strong>Organization:</strong>		".@$data['org']."<br />";
				$msg .= "<strong>Title:</strong>			".@$data['title']."<br />";
				$msg .= "<strong>First Name:</strong>		".@$data['fname']."<br />";
				$msg .= "<strong>Last Name:</strong>		".@$data['lname']."<br />";
				$msg .= "<strong>Address:</strong>			".@$data['adr']."<br />";
				$msg .= "<strong>City:</strong>				".@$data['city']."<br />";
				$msg .= "<strong>State:</strong>			".@$data['state']."<br />";
				$msg .= "<strong>Zip:</strong>				".@$data['zip']."<br />";
				$msg .= "<strong>Phone:</strong>			".@$data['phone']."<br />";
				$msg .= "<strong>E-Mail:</strong>			".@$data['email']."<br />";
				$msg .= "<strong>Newsletter:</strong>		".$newsletter."<br />";
				$msg .= "<strong>Comments:</strong><br>		".@$data['comments']."<br />";

				if($cont['contact']=="report") {
					$msg .= "<br><strong>Report:</strong> ".$rowrep->name."<br />";
				} else {
					$msg .= "<br><strong>From Page:</strong> http://".$_SERVER['HTTP_HOST']."".$_SERVER['REQUEST_URI']."<br />";
				}

				$msg .= "<br />".$_SERVER['REMOTE_ADDR']."<br />";
				$msg .= "------------------------------------------------------";

				$mail = new Zend_Mail();
				$mail->setType(Zend_Mime::MULTIPART_RELATED);
				$mail->setBodyHtml($msg);
				$mail->setFrom(@$data['email'], @$data['fname']." ".@$data['lname']);
				$mail->addTo(array("arremarci@gmail.com","webmaster@aboutroatanrealestate.com"));
				$mail->setSubject('Contact Form ['.date('d/M/Y').']');
				$mail->send();

				if($cont['contact']=="report") {

					// sent report to user
					$emsg = str_replace("%NAME%", $data['fname']." ".$data['lname'], $rowrep->email_cont);
					$emsg = str_replace("%REPORT_ADR%", "<a href=\"http://".$_SERVER['HTTP_HOST']."/report/".$rowrep->id."\">http://".$_SERVER['HTTP_HOST']."/report/".$rowrep->id."</a>", $emsg);

					$mail = new Zend_Mail();
					$mail->setType(Zend_Mime::MULTIPART_RELATED);
					$mail->setBodyHtml($emsg);
					$mail->setFrom("office@aboutroatanrealestate.com", "About Roatan Real Estates");
					$mail->addTo($data['email']);
					$mail->setSubject('About Roatan Real Estates Report: '.$rowrep->name);
					$mail->send();

				}

				return $cont['thankyou_cont'];

			}

		}

		$form_html = '';

		if($cont['contact']=="standard") {
			if($sw==1) {
				$err_msg = '
				<table width="90%" border="0" cellpadding="4" cellspacing="1" bgcolor="#FF0000" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000000">
					<tr>
						<td colspan="2" bgcolor="#FFCCCC">'.$err_txt.' '.$err_email.' '.$err_code.'</td>
					</tr>
				</table><br />';
			}
			$form_html = '
			<form id="form1" name="form1" method="post" action="" data-abide>
				<div class="row">
					<div class="columns">
						<p><strong>Your Contact Information (Please complete all fields)</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="fname">First Name:</label>
								<input name="fname" type="text" id="fname" value="" required />
								<small class="error">Please enter your first name.</small>
							</li>
							<li>
								<label for="lname">Last Name:</label>
								<input name="lname" type="text" id="lname" value="" required />
								<small class="error">Please enter your last name.</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="adr">Street Address:</label>
								<input name="adr" type="text" id="adr" value="" />
							</li>
							<li>
								<label for="city">City:</label>
								<input name="city" type="text" id="city" value="" />
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="state">State/Province:</label>
								<input name="state" type="text" id="state" value="" />
							</li>
							<li>
								<label for="zip">Zip/Postal Code:</label>
								<input name="zip" type="text" id="zip" value="" />
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="phone">Phone:</label>
								<input name="phone" type="tel" id="phone" value="" />
							</li>
							<li>
								<label for="email">Email:</label>
								<input name="email" type="email" id="email" value="" required />
								<small class="error">Please enter a valid email address</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<label for="comments">Comments:</label>
						<textarea name="comments" rows="5" id="comments"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						'.$recaptcha->getHTML().'<br />
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<label for="newsletter"><input name="newsletter" type="checkbox" id="newsletter" value="yes" /> I wish to receive your email announcements &amp; newsletter.</label>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<button type="submit">Submit</button>
					</div>
				</div>
			</form>';
		}

		if($cont['contact']=="b2b") {
			if($sw==1) {
				$err_msg = '
				<table width="90%" border="0" cellpadding="4" cellspacing="1" bgcolor="#FF0000" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000000">
					<tr>
						<td colspan="2" bgcolor="#FFCCCC">'.$err_txt.' '.$err_email.' '.$err_code.'</td>
					</tr>
				</table><br />';
			}
			$form_html = '
			<form id="form2" name="form2" method="post" action="" data-abide>
				<div class="row">
					<div class="columns">
						<p><strong>Your Contact Information (Please complete all fields)</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="org">Organization:</label>
								<input name="org" type="text" id="org" value="" />
							</li>
							<li>
								<label for="title">Title:</label>
								<input name="title" type="text" id="title" value="" required />
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="fname">First Name:</label>
								<input name="fname" type="text" id="fname" value="" required />
								<small class="error">Please enter your first name.</small>
							</li>
							<li>
								<label for="lname">Last Name:</label>
								<input name="lname" type="text" id="lname" value="" required />
								<small class="error">Please enter your last name.</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="adr">Street Address:</label>
								<input name="adr" type="text" id="adr" value="" />
							</li>
							<li>
								<label for="city">City:</label>
								<input name="city" type="text" id="city" value="" />
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="state">State/Province:</label>
								<input name="state" type="text" id="state" value="" />
							</li>
							<li>
								<label for="zip">Zip/Postal Code:</label>
								<input name="zip" type="text" id="zip" value="" />
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="phone">Phone:</label>
								<input name="phone" type="tel" id="phone" value="" />
							</li>
							<li>
								<label for="email">Email:</label>
								<input name="email" type="email" id="email" value="" required />
								<small class="error">Please enter a valid email address</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<label for="comments">Comments:</label>
						<textarea name="comments" rows="5" id="comments"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						'.$recaptcha->getHTML().'<br />
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<label for="newsletter"><input name="newsletter" type="checkbox" id="newsletter" value="yes" /> I wish to receive your email announcements &amp; newsletter.</label>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<button type="submit">Submit</button>
					</div>
				</div>
			</form>';
		}

		if($cont['contact']=="report") {

			$repsel = '<select name="report">';
			$repsel .= '<option value="">Please Select</option>';
			$rez = $robj->getSelReports($cont['id']);
			foreach($rez as $row) {
				$repsel .= '<option value="'.$row->id.'">'.$row->name.'</option>';
			}
			$repsel .= '</select>';

			if($sw==1) {
				$err_msg = '
				<table width="90%" border="0" cellpadding="4" cellspacing="1" bgcolor="#FF0000" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#000000">
					<tr>
						<td colspan="2" bgcolor="#FFCCCC">'.$err_txt.' '.$err_email.' '.$err_code.' '.$err_rept.' '.$err_news.'</td>
					</tr>
				</table><br />';
			}
			$form_html = '
			<form id="form3" name="form3" method="post" action="" data-abide>
				<div class="row">
					<div class="columns">
						<p><strong>Your Contact Information (Please complete all fields)</strong></p>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="fname">First Name:</label>
								<input name="fname" type="text" id="fname" value="" required />
								<small class="error">Please enter your first name.</small>
							</li>
							<li>
								<label for="lname">Last Name:</label>
								<input name="lname" type="text" id="lname" value="" required />
								<small class="error">Please enter your last name.</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="adr">Street Address:</label>
								<input name="adr" type="text" id="adr" value="" />
							</li>
							<li>
								<label for="city">City:</label>
								<input name="city" type="text" id="city" value="" />
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="state">State/Province:</label>
								<input name="state" type="text" id="state" value="" />
							</li>
							<li>
								<label for="zip">Zip/Postal Code:</label>
								<input name="zip" type="text" id="zip" value="" />
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<ul class="medium-block-grid-2">
							<li>
								<label for="phone">Phone:</label>
								<input name="phone" type="tel" id="phone" value="" />
							</li>
							<li>
								<label for="email">Email:</label>
								<input name="email" type="email" id="email" value="" required />
								<small class="error">Please enter a valid email address</small>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<label for="comments">Comments:</label>
						<textarea name="comments" rows="5" id="comments"></textarea>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<label for="report">Report:</label>
						'.$repsel.'
					</div>
				</div>
				<div class="row">
					<div class="columns">
						'.$recaptcha->getHTML().'<br />
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<label for="newsletter"><input name="newsletter" type="radio" id="newsletter" value="yes" /> I wish to receive your email announcements &amp; newsletter.</label>
						<label for="only_now"><input name="newsletter" type="radio" id="only_now" value="no" /> Please only send email for this request.</label>
					</div>
				</div>
				<div class="row">
					<div class="columns">
						<button type="submit">Submit</button>
					</div>
				</div>
			</form>';
		}

		return $form_html;

	}

}