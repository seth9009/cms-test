<?php

class Zend_View_Helper_LoadDomainStyle {

	public function LoadDomainStyle() {

		$html = 'style.css';

		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'northridgesabetha.css';
		}

		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'northridgeseneca.css';
		}

		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'northridgenebraska.css';
		}

		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$html = 'northridgehumboldt.css';
		}

		return $html;

	}

}
