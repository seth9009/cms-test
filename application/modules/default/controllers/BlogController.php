<?php

class BlogController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('blogs');
		Zend_Loader::loadClass('comments');
		Zend_Loader::loadClass('Zend_Filter_StripTags');
		Zend_Loader::loadClass('Zend_Paginator');

	}

	function indexAction() {

		$obj = new Blogs();
		$result = $obj->getFrontEndPosts(10);
		$this->view->results = $result;

		$this->view->assign('title', 'Latest Blog Posts');

	}


	// view record
	function viewAction() {

		$obj = new Blogs();
		$cobj = new Comments();
		$request = $this->getRequest();

		$permalink = $request->getParam('permalink');

		if($request->isPost()) {
			if(!$_POST['email'] && $_POST['author'] && $_POST['subject'] && $_POST['message']) {
				$cobj->saveComment($request->getPost());
				$this->view->postcom = 1;
			}
		}

		if($permalink) {
			$post = $obj->getPostByPermalink($permalink);
			$this->view->post = $post;
			$comments = $cobj->getCommentsByPostId($post['id'],'1');
			$this->view->comments = $comments;
		} else {
			$this->_redirect("/blog");
		}

		$this->view->assign('title', $post['title']);

	}

}