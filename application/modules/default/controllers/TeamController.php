<?php
class TeamController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('teams');

	}

	function indexAction() {

		$this->_redirect("/");

	}


	function viewAction() {

		$obj = new Teams();
		$this->_helper->layout->disableLayout();

		$id = $this->_request->getParam('id');
		if(!empty($id)) {
			$this->view->row = $obj->getMember($id);
		} else {
			$this->_redirect("/");
		}

	}

}