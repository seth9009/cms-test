<?php
class ReportController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('reports');

	}

	function indexAction() {

		$this->_redirect("/");

	}


	function viewAction() {

		$obj = new Reports();

		$id = $this->_request->getParam('id');

		if(!empty($id)) {
			$row = $obj->getById($id);
			$this->view->row = $row;
		} else {
			$this->_redirect("/");
		}

		$this->view->assign('title', $row['name']);

	}

}