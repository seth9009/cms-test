<?php
class PageController extends Zend_Controller_Action {

	function init() {

		$this->view->baseUrl = $this->_request->getBaseUrl();
		Zend_Loader::loadClass('pages');
		Zend_Loader::loadClass('sliders');
		Zend_Loader::loadClass('banners');

	}

	function indexAction() {

		$obj = new Sliders();
		$bobj = new Banners();

		$this->view->slider = $obj->getSliderBySite();
		$this->view->banners = $bobj->getHomeBannersBySite();

	}


	function viewAction() {

		$pobj = new Pages();

		$permalink = $this->_request->getParam('permalink');

		if(!empty($permalink)) {
			$page = $pobj->getPageByPerma($permalink);
			if($page) {
				$this->view->page = $page;
			} else {
				throw new Zend_Controller_Action_Exception('This page does not exist', 404);
			}
		} else {
			$this->_redirect("/");
		}

		$this->view->assign('metakeys', $page['seo_keywords']);
		$this->view->assign('metadesc', $page['seo_description']);
		$this->view->assign('title', $page['title']);

	}

}