<?php

class ErrorController extends Zend_Controller_Action {

    public function errorAction() {

		$this->view->assign('title', "Page does not exist");
        $this->getResponse()->setHttpResponseCode(404);

    }

}
