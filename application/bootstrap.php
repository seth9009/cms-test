<?php
require_once"Zend/Loader/Autoloader.php";

class Bootstrap {

	public static $frontController = null;
	public static $root = "";
	public static $registry = null;

	public static function run() {

		self::prepare();
		$response = self::$frontController->dispatch();
		self::sendResponse($response);

	}

	public static function setupEnvironment() {

		error_reporting(E_ALL|E_STRICT);
		ini_set('display_errors', false);
		date_default_timezone_set('Europe/Paris');
		self::$root = dirname(dirname(__FILE__));

	}

	public static function prepare() {

		self::setupEnvironment();
		$loader = Zend_Loader_Autoloader::getInstance();
		$loader->registerNamespace('My');
		self::setupRegistry();
		self::setupConfiguration();
		self::setupFrontController();
		self::setupView();
		self::setupDatabase();
		self::setupRoutes();
		self::setupSession();
		self::setupPlugins();
		self::setupErrorHandler();

	}

	public static function setupFrontController() {

		self::$frontController = Zend_Controller_Front::getInstance();
		self::$frontController->throwExceptions(true);
		self::$frontController->returnResponse(true);
		self::$frontController->addModuleDirectory(self::$root.'/application/modules');
		self::$frontController->setControllerDirectory(array(
															'default' => self::$root.'/application/modules/default/controllers',
															'admin'   => self::$root.'/application/modules/admin/controllers'
															));
		self::$frontController->setParam('registry', self::$registry);
		$response = new Zend_Controller_Response_Http; // Set default Content-Type
		$response->setHeader('Content-Type', 'text/html; charset=UTF-8', true);
		self::$frontController->setResponse($response);

	}

	public static function setupView() {

		$view = new Zend_View(array('encoding'=>'UTF-8'));
		$viewRendered = new Zend_Controller_Action_Helper_ViewRenderer($view);
		Zend_Controller_Action_HelperBroker::addHelper($viewRendered);

        Zend_Layout::startMvc(
            array(
                'layoutPath' => self::$root . '/application/layouts',
                'layout' => 'default'
            )
        );

    }

	public static function sendResponse(Zend_Controller_Response_Http $response) {

		$response->sendResponse();

	}

	public static function setupRegistry() {

		self::$registry = new Zend_Registry(array(), ArrayObject::ARRAY_AS_PROPS);
		Zend_Registry::setInstance(self::$registry);

	}

	public static function setupConfiguration() {

		$config = new Zend_Config_Ini("config/config.ini", 'general');
		self::$registry->configuration = $config;

	}

	public static function setupDatabase() {

		$config = self::$registry->configuration;
		$db = Zend_Db::factory($config->db->adapter, $config->db->config->toArray());
		self::$registry->db = $db;
		Zend_Db_Table::setDefaultAdapter($db);

	}

	public static function setupSession() {

		Zend_Session::start();

	}

	public static function setupPlugins() {

		self::$frontController->registerPlugin(new My_Controller_Plugin_Login());
		self::$frontController->registerPlugin(new My_Controller_Plugin_Layout());

	}

	public static function setupErrorHandler() {

		self::$frontController->throwExceptions(false);
		self::$frontController->registerPlugin(new Zend_Controller_Plugin_ErrorHandler(array(
																						'module'     => 'default',
																						'controller' => 'error',
																						'action'     => 'error')
																						));

	}

	public static function setupRoutes() {

		$router = self::$frontController->getRouter();

		$route = new Zend_Controller_Router_Route_Regex(
			'page/(.*)',
			array(
				'controller' => 'page',
				'action'     => 'view'
			),
			array(
				1 => 'permalink'
			),
			'page/%s'
		);
		$router->addRoute('page', $route);

		$route = new Zend_Controller_Router_Route_Regex(
			'blog/(.*)',
			array(
				'controller' => 'blog',
				'action'     => 'view'
			),
			array(
				1 => 'permalink'
			),
			'blog/%s'
		);
		$router->addRoute('blog', $route);

		$route = new Zend_Controller_Router_Route_Regex(
			'report/(.*)',
			array(
				'controller' => 'report',
				'action'     => 'view'
			),
			array(
				1 => 'id'
			),
			'report/%s'
		);
		$router->addRoute('report', $route);

	}

}