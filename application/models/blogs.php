<?php

class Blogs extends Zend_Db_Table {

	protected $_name = 'blog';

	// get all blogs
	public function getPosts($status = 0) {
		$select = $this->select();
		if($status) {
			$select->where('status = ?', '1');
		}
		return $this->fetchAll($select);
	}

	// get blog by id
	public function getPostById($id) {
		$select = $this->select()->where('id = ?', $id);
		return $this->fetchRow($select);
	}

	// get blog by permalink
	public function getPostByPermalink($permalink) {
		$select = $this->select()->where('permalink = ?', $permalink);
		return $this->fetchRow($select);
	}

	// get font page blogs
	public function getFrontEndPosts($limit) {

		$comments = "(SELECT COUNT(*) FROM comments WHERE comments.blogId = blog.id AND comments.status=1)";

		$select = $this->select();
		$select->from('blog', array('*','comments' => new Zend_Db_Expr($comments)));
		$select->where('status = ?', '1');
		$select->where('publish_date <= ?', time());
		if($limit) {
			$select->limit($limit);
		}
		$select->order('publish_date DESC');
//		echo $select->__toString();

		return $this->fetchAll($select);

	}

	// save blog
	public function savePost($post) {

		$dateObj = new Zend_Date($post['date']);
		$mktime = $dateObj->getTimestamp();

		$f = new Zend_Filter_StripTags();
		$data = array(
			'title'			=> $f->filter($post['title']),
			'permalink'		=> $this->makePermalink($post['permalink']),
			'author'		=> $f->filter($post['author']),
			'content'		=> $post['content'],
			'publish_date'	=> $mktime,
			'status'		=> $post['status']
		);

		if(@$post['id']) {
			$this->update($data,'id = '.$post['id']);
			return $post['id'];
		} else {
			$this->insert($data);
			return $this->_db->lastInsertId();
		}

	}

	// delete blog
	public function delPost($id) {

		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);

	}

	private function makePermalink($name) {

		$name = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $name);
		$name = strtolower(trim($name));
		$name = preg_replace("/[\/_|+ -]+/", '-', $name);

		return $name;

	}

}