<?php

class Fields extends Zend_Db_Table {

	protected $_name = 'forms_fields';

	// get one
	public function getField($id) {
		$select = $this->select()->where('id = ?', $id);
		return $this->fetchRow($select);
	}


	// get all fields in a form
	public function getFormFields($id) {
		$select = $this->select()->where('formId = ?', $id);
		return $this->fetchAll($select);
	}

	// save 
	public function saveField($post) {

		$f = new Zend_Filter_StripTags();
        $data = array(
			'formId'        => $f->filter($post['formId']),
            'type'          => $f->filter($post['type']),
            'name'          => $f->filter($post['name']),
            'description'   => $f->filter($post['description']),
            'options'       => $f->filter($post['options']),
            'required'      => $f->filter($post['required']),
            'rank'          => $f->filter($post['rank'])
        );

		if(@$post['id']) {
			$this->update($data,'id = '.$post['id']);
			return $post['id'];
		} else {
			$this->insert($data);
			return $this->_db->lastInsertId();
		}

	}

	// delete
	public function deleteFieldsByFormId($id) {
		$where = $this->getAdapter()->quoteInto('formId = ?', $id);
		$this->delete($where);
	}

}