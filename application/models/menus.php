<?php

class Menus extends Zend_Db_Table {

	protected $_name = 'menu';

	// get menus by id
	public function getMenuById($id) {

		$site = 'northridgesabetha.com';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgesabetha.com'; }
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgeseneca.com'; }
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgenebraska.com'; }
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgehumboldt.com'; }

		$select = $this->select();
		$select->where('id = ?', $id);
		if($site) {
			$select->where('site = ?', $site);
		}
		// echo $select->__toString(); exit;
		return $this->fetchRow($select);

	}

	// get menus by site
	public function getMenuBySite() {

		$site = 'northridgesabetha.com';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgesabetha.com'; }
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgeseneca.com'; }
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgenebraska.com'; }
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgehumboldt.com'; }

		$select = $this->select();
		$select->where('site = ?', $site);
		$select->order('rank ASC');
		// echo $select->__toString(); exit;
		return $this->fetchAll($select);

	}

	// get menus by parent id
	public function getMenusByParentId($id) {

		$site = 'northridgesabetha.com';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgesabetha.com'; }
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgeseneca.com'; }
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgenebraska.com'; }
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgehumboldt.com'; }

		$select = $this->select();
		$select->where('parent_id = ?', $id);
		if($site) {
			$select->where('site = ?', $site);
		}
		$select->order('rank ASC');
		return $this->fetchAll($select)->toArray();

	}

	// get id by path
	public function getIdByPath($id) {

		$site = 'northridgesabetha.com';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgesabetha.com'; }
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgeseneca.com'; }
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgenebraska.com'; }
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgehumboldt.com'; }

		$select = $this->select();
		$select->where('path = ?', $id);
		if($site) {
			$select->where('site = ?', $site);
		}
		//echo $select->__toString(); exit;
		return $this->fetchRow($select);

	}

	// get max id
	public function getMaxId() {

		$select = $this->select()
					->order('id DESC');
		$row = $this->fetchRow($select);

		if(count($row)>0) {
			return $row->id;
		} else {
			return 0;
		}

	}

	// make admin meniu
	public function makeAdminMenu($id,$level,$site) {

		$select = $this->select()->where('parent_id = ?', $id);
		$select->where('site = ?', $site);
		$select->order('rank ASC');
		//echo $select->__toString(); exit;

		$menus = $this->fetchAll($select);

		$showmenu = "";
		if($level==0) {
			$showmenu = "<ol class=\"sortable\">";
		} else {
			if(count($menus)>0) {
				$showmenu = "<ol>";
			}
		}
		foreach($menus as $menu) {
			$showmenu .= '
				<li id="menu-item-'.str_replace(" ","_",$menu->name).'['.$menu->path.']-'.$menu->id.'" class="item-'.$menu->id.'">
					<dl class="menu-item-bar">
						<dt class="menu-item-handle">
							<span class="item-title">'.$menu->name.'</span>
							<span class="item-controls">
								<span class="item-type"><a class="remove" id="item-'.$menu->id.'">Delete</a></span>
							</span>
						</dt>
					</dl>';
			$showmenu .= $this->makeAdminMenu($menu->id,$level+1,$site);
		}
		if(count($menus)>0) {
			$showmenu .= "</ol>";
		}

		return $showmenu;

	}


	// save menu
	public function saveMenu($post) {

		$site = $post['site'];

		// delete menu
		$this->delMenus($site);

		// insert the new selection
		$string = substr($post['ser'],9);
		$mex = explode("&menu-item",$string);

		foreach($mex as $key => $men) {
			$this->insertMenu($men,$site,$key);
		}

	}

	// insert menu item
	public function insertMenu($menu,$site,$rank) {

		$mex = explode("=",$menu);
		($mex[1]=="root")?$pid=0:$pid=$mex[1];

		preg_match('/\-(.*)\[(.*)\]\[(.*)\]/iU',$mex[0],$mvar);

		$type = 'page';
		if(!is_numeric($mvar[2])) {
			$type = 'link';
		}

		$data = array(
			'id' => $mvar[3],
			'parent_id' => $pid,
			'name' => str_replace("_"," ",$mvar[1]),
			'path' => $mvar[2],
			'type' => $type,
			'rank' => $rank,
			'site' => $site,
		);
		$this->insert($data);

	}

	// delete menu by path
	public function delMenuByPath($path,$site) {

		$where = array();
		$where[] = $this->getAdapter()->quoteInto('site = ?', $site);
		$where[] = $this->getAdapter()->quoteInto('path = ?', $path);
		$this->delete($where);

	}

	// delete menus
	public function delMenus($type) {

		$where = $this->getAdapter()->quoteInto('site = ?', $type);
		$this->delete($where);

	}


}
