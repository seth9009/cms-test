<?php

Zend_Loader::loadClass('fields');

class Forms extends Zend_Db_Table {

	protected $_name = 'forms';

	// get row
	public function getForm($id) {
		$select = $this->select()->where('id = ?', $id);
		return $this->fetchRow($select);
	}


	// save 
	public function saveForm($post) {

		$f = new Zend_Filter_StripTags();
		$data = array(
            'name'            	=> $f->filter($post['name']),
            'subject'			=> $f->filter($post['subject']),
			'emailto'			=> $f->filter($post['emailto']),
			'message_type'		=> $f->filter($post['message_type']),
			'message'			=> $f->filter($post['message'])
        );

        if(@$post['id']) {
            $this->update($data,'id = '.$post['id']);
            return $post['id'];
        } else {
            $this->insert($data);
            return $this->_db->lastInsertId();
        }

	}

	// delete row
	public function delForm($id) {
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);
	}


    public function BuildForm($id,$fields,$data = false) {

    	$fields = new Fields();

        if($data) {

            $form = $this->getForm($id);

            $mailData = '';
            foreach ($data['fields'] as $key => $post) {
                $field = $fields->getField($key);
                if(is_array($post)) {
                    $postData = implode(",", $post);
                } else {
                    $postData = $post;
                }
                $mailData .= '<b>'.$field->name.':</b> '.$postData."<br />";
            }

			$mail = new Zend_Mail();
			$mail->setType(Zend_Mime::MULTIPART_RELATED);
			$mail->setBodyHtml($mailData);
			$mail->setFrom('no-reply@'.$_SERVER['SERVER_NAME'], $_SERVER['SERVER_NAME']);
			$mail->addTo($form->emailto);
			$mail->setSubject($form->subject.' ['.date('d/M/Y').']');
			$mail->send();

            $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

			$html = '';
			if($form->message_type=="text") {
				$html .= '<b>'.$form->message.'</b>';
			}
			if($form->message_type=="page") {
				$redirector->gotoUrl("/page/".$form->message);
			}
			if($form->message_type=="redirect") {
				$redirector->gotoUrl($form->message);
			}

			return $html;

		}

        if(is_numeric($id)){

            $html = '<form method="post">';
            foreach($fields->getFormFields($id) as $field) {

                $required = '';
                if($field->required==1) {
                    $required = 'required';
                }

                $html .= '<div class="row" id="row'.$field->id.'">';
                $html .= '<label for="field'.$field->id.'">'.$field->name.'</label>';
                if($field->description) {
                    $html .= '<p>'.$field->description.'</p>';
                }
                if($field->type=="Text") {
                    $html .= '<input type="text" id="field'.$field->id.'" name="fields['.$field->id.']" '.$required.'>';
                }
                if($field->type=="Textarea") {
                    $html .= '<textarea rows="5" id="field'.$field->id.'" name="fields['.$field->id.']" '.$required.'></textarea>';
                }
                if($field->type=="Select") {
                    $options = unserialize($field->options);
                    $html .= '<select name="fields['.$field->id.']" id="field'.$field->id.'" '.$required.'>';
                    $html .= '<option value="">Please Select</option>';
                    foreach($options as $option) {
                        $html .= '<option value="'.$option.'">'.$option.'</option>';
                    }
                    $html .= '</select>';
                }
                if($field->type=="Checkbox") {
                    $options = unserialize($field->options);
                    foreach($options as $key => $option) {
                        $html .= '<input name="fields['.$field->id.'][]" type="checkbox" id="opt'.$field->id.$key.'" value="'.$option.'"> <label class="inlinelabel" for="opt'.$field->id.$key.'">'.$option.'</label><br />';
                    }
                }
                if($field->type=="Radio") {
                    $options = unserialize($field->options);
                    foreach($options as $key => $option) {
                        $html .= '<input name="fields['.$field->id.']" type="radio" id="opt'.$field->id.$key.'" value="'.$option.'"> <label class="inlinelabel" for="opt'.$field->id.$key.'">'.$option.'</label><br />';
                    }
                }
                $html .= '</div>';
            }
            $html .= '<div class="row">';
            $html .= '<input type="submit" id="submit" name="submit" value="Submit">';
            $html .= '</div>';
            $html .= '</form>';

            return $html;

        } else {

            return false;

        }

    }

}