<?php

class Comments extends Zend_Db_Table {

	protected $_name = 'comments';

	// get comments by id
	public function getCommentsById($id) {
		$select = $this->select()->where('id = ?', $id);
		return $this->fetchRow($select);
	}

	// get comments by blog id
	public function getCommentsByPostId($id,$status) {
		$select = $this->select()->where('blogId = ?', $id);
		if($status) {
			$select->where('status = ?',$status);
		}
		return $this->fetchAll($select);
	}

	// save comment
	public function saveComment($post) {

		$f = new Zend_Filter_StripTags();
		$data = array(
			'blogId'	=> $f->filter($post['bid']),
			'author'	=> ($post['author'])?$f->filter($post['author']):'',
			'subject'	=> ($post['subject'])?$f->filter($post['subject']):'',
			'message'	=> ($post['message'])?$post['message']:'',
			'regdate'	=> time(),
			'status'	=> 0
		);

		if(@$post['id']) {
			$this->update($data,'id = '.$post['id']);
			return $post['id'];
		} else {
			$this->insert($data);
			return $this->_db->lastInsertId();
		}

	}

	public function updateComment($id,$status) {

		$data = array(
			'status' => $status
		);
		$this->update($data,'id = '.$id);

	}

	// delete comment
	public function delComment($id) {

		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);

	}

}