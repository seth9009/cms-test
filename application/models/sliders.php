<?php

class Sliders extends Zend_Db_Table {

	protected $_name = 'slider';

	// get image by id
	public function getImageById($id) {
		$select = $this->select()->where('id = ?', $id);
		return $this->fetchRow($select);
	}

	// get slider by site
	public function getSliderBySite() {

		$site = 'northridgesabetha.com';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgesabetha.com'; }
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgeseneca.com'; }
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgenebraska.com'; }
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgehumboldt.com'; }

		$select = $this->select();
		$select->where('site = ?', $site);
		$select->where('status = ?', '1');
		$select->order('rank ASC');
		return $this->fetchAll($select);

	}

	// save
	public function save($post) {

		$f = new Zend_Filter_StripTags();
		$data = array(
								'link'        => $f->filter($post['link']),
								'site'				=> $f->filter($post['site']),
								'rank'				=> $f->filter($post['rank']),
								'status'			=> $f->filter($post['status'])
    );
		if($post['image']) {
			$data['image'] = $post['image'];
		}

		if(@$post['id']) {
			$this->update($data,'id = '.$post['id']);
			return $post['id'];
		} else {
			$this->insert($data);
			return $this->_db->lastInsertId();
		}

	}

	// delete
	public function del($id) {
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);
	}

}
