<?php

class Teams extends Zend_Db_Table {

	protected $_name = 'team';


	// get all records
	public function getAll() {
		return $this->fetchAll();
	}

	// get team members by site
	public function getMemberBySite($site) {
		$select = $this->select();
		$select->where('site = ?', $site);
		$select->order('rank ASC');
		return $this->fetchAll($select);
	}

	// get team member by id
	public function getMember($id) {
		$select = $this->select()->where('id = ?', $id);
		return $this->fetchRow($select);
	}


	// save member
	public function saveMember($post) {

		$f = new Zend_Filter_StripTags();
		$data = array(
            'name'            	=> $f->filter($post['name']),
            'position'          => $f->filter($post['position']),
            'email'				=> $f->filter($post['email']),
			'bio'				=> $post['bio'],
			'site'				=> $f->filter($post['site']),
			'rank'				=> $f->filter($post['rank']),
			'status'			=> $f->filter($post['status'])
        );
		if($post['pic']) {
			$data['pic'] = $post['pic'];
		}
		if($post['main_pic']) {
			$data['main_pic'] = $post['main_pic'];
		}

		if(@$post['id']) {
			$this->update($data,'id = '.$post['id']);
			return $post['id'];
		} else {
			$this->insert($data);
			return $this->_db->lastInsertId();
		}

	}

	// delete member
	public function delMember($id) {
		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);
	}

    public function BuildTeam() {

		$baseUrl = Zend_Controller_Front::getInstance()->getRequest()->getBaseUrl();

		$site = 'northridgesabetha.com';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgesabetha.com'; }
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgeseneca.com'; }
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgenebraska.com'; }
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgehumboldt.com'; }

		$sites = array('northridgesabetha.com'=>'Sabetha','northridgeseneca.com'=>'Seneca','northridgenebraska.com'=>'Nebraska','northridgehumboldt.com'=>'Humboldt');

        $rez = $this->getMemberBySite($site);
        $html = '<ul class="medium-block-grid-4 small-block-grid-1 team">';
        foreach($rez as $row) {
			$html .= '<li>';
			$html .= '<a data-reveal-id="team-'.$row->id.'" href="#"><img src="'.$baseUrl.'/upload/team/'.$row->pic.'" alt="'.$row->name.'" /><strong>'.$row->name.'</strong>'.$row->position.'</a>';
			$html .= '<div id="team-'.$row->id.'" class="reveal-modal medium" data-reveal aria-hidden="true" role="dialog">';
			$html .= '<h2>'.$row->name.', '.$row->position.'</h2>
					<table width="100%">
						<tr>
							<td width="525" valign="top">';
							if($row->main_pic) {
								$html .= '<img src="'.$baseUrl.'/upload/team/'.$row->main_pic.'">';
							} else {
								$html .= '<img src="'.$baseUrl.'/upload/team/'.$row->pic.'">';
							}
			$html .= '
							</td>
							<td valign="top">'.$row->bio.'<hr /><a href="mailto:'.$row->email.'">'.$row->email.'</a>							</td>
						</tr>
					</table>';
			$html .= '<a class="close-reveal-modal" aria-label="Close">&#215;</a>';
			$html .= '</div>';
			$html .= '</li>';
        }
        $html .= '</ul>';

		unset($sites[$site]);

		foreach ($sites as $key => $val) {
			$rez = $this->getMemberBySite($key);
			if(count($rez) > 0) {
				$html .= '<h1>'.$val.'</h1>';
				$html .= '<ul class="medium-block-grid-4 small-block-grid-1 team">';
				foreach($rez as $row) {
					$html .= '<li>';
					$html .= '<a data-reveal-id="team-'.$row->id.'" href="#"><img src="'.$baseUrl.'/upload/team/'.$row->pic.'" alt="'.$row->name.'" /><strong>'.$row->name.'</strong>'.$row->position.'</a>';
					$html .= '<div id="team-'.$row->id.'" class="reveal-modal medium" data-reveal aria-hidden="true" role="dialog">';
					$html .= '<h2>'.$row->name.', '.$row->position.'</h2>
							<table width="100%">
								<tr>
									<td width="525" valign="top">';
									if($row->main_pic) {
										$html .= '<img src="'.$baseUrl.'/upload/team/'.$row->main_pic.'">';
									} else {
										$html .= '<img src="'.$baseUrl.'/upload/team/'.$row->pic.'">';
									}
					$html .= '
									</td>
									<td valign="top">'.$row->bio.'<hr /><a href="mailto:'.$row->email.'">'.$row->email.'</a>							</td>
								</tr>
							</table>';
					$html .= '<a class="close-reveal-modal" aria-label="Close">&#215;</a>';
					$html .= '</div>';
					$html .= '</li>';
				}
				$html .= '</ul>';
			}
		}

        return $html;

    }

}
