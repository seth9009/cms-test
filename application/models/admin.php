<?php

class Admin extends Zend_Db_Table {

	protected $_name = 'admin';

    public function getUser($id) {

        return $this->fetchRow('id = '.$id);

    }

    public function getUsers() {

		return $this->fetchAll();

    }

	public function delUser($id) {

		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);

	}

    public function saveUser($post) {

        $data['user'] = $post['user'];
        if($post['pass']) {
        	$data['pass'] = md5($post['pass']);
        }

        if($post['id']) {
			$this->update($data,'id = '.$post['id']);
        } else {
			$this->insert($data);        	
        }

    }

}