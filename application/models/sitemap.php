<?php

Zend_Loader::loadClass('pages');
Zend_Loader::loadClass('blogs');

class Sitemap {

	function generate() {

		$obj = new Pages();
		$bobj = new Blogs();

		$fh = fopen("sitemap.xml", 'w') or die("can't open file");

	 	$time = date("Y-m-d")."T".date("H:i:s+00:00");

		$siteurl = "http://".$_SERVER['HTTP_HOST'];

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
		<url>
			<loc>'.trim($siteurl).'/</loc>
			<lastmod>'.$time.'</lastmod>
			<changefreq>monthly</changefreq>
			<priority>1.0</priority>
		</url>';

		$pages = $obj->getPages();
		foreach($pages as $page) {
			$xml .= '
		<url>
			<loc>'.trim($siteurl).'/page/'.$page["permalink"].'</loc>
			<lastmod>'.$time.'</lastmod>
			<changefreq>yearly</changefreq>
			<priority>0.5</priority>
		</url>';
		}

		$xml .='</urlset>';

		fwrite($fh, $xml);
		fclose($fh);

	}

}