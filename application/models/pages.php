<?php

class Pages extends Zend_Db_Table {

	protected $_name = 'pages';


	// get all pages
	public function getPages() {
		return $this->fetchAll();
	}


	// get page by id
	public function getPageById($id) {
		$select = $this->select()->where('id = ?', $id);
		return $this->fetchRow($select);
	}


	// get pages by site
	public function getPagesBySite($site) {
		$select = $this->select();
		$select->where('site = ?', $site);
		$select->orWhere('site = ?', '');
		//echo $select->__toString();
		return $this->fetchAll($select);
	}


	// get page by permalink
	public function getPageByPerma($perma) {

		$site = 'northridgesabetha.com';
		if(preg_match("/northridgesabetha\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgesabetha.com'; }
		if(preg_match("/northridgeseneca\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgeseneca.com'; }
		if(preg_match("/northridgenebraska\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgenebraska.com'; }
		if(preg_match("/northridgehumboldt\.com/i", $_SERVER['HTTP_HOST'])) {
			$site = 'northridgehumboldt.com'; }

		$select = $this->select();
		$select->where('permalink = ?', $perma);
		$select->where("site = '$site' OR site = ''");
		$select->where('status = ?', '1');
		return $this->fetchRow($select);
	}


	// save page
	public function savePage($post) {

		$f = new Zend_Filter_StripTags();
		$data = array(
			'site'				=> $f->filter($post['site']),
			'title'				=> $f->filter($post['title']),
			'permalink'			=> $this->makePermalink($post['title']),
			'seo_keywords'		=> $f->filter($post['seo_keywords']),
			'seo_description'	=> $f->filter($post['seo_description']),
			'content'			=> $post['content'],
			'contact'			=> $f->filter($post['contact']),
			'thankyou_cont'		=> $post['thankyou_cont'],
			'status'			=> $post['status']
		);
		if($post['header']) {
			$data['header'] = $post['header'];
		}

		if(@$post['id']) {
			$this->update($data,'id = '.$post['id']);
			return $post['id'];
		} else {
			$data['publish_date'] = time();
			$this->insert($data);
			return $this->_db->lastInsertId();
		}

	}

	// delete page
	public function delPage($id) {

		$where = $this->getAdapter()->quoteInto('id = ?', $id);
		$this->delete($where);

	}

	private function makePermalink($name) {

		$name = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $name);
		$name = strtolower(trim($name));
		$name = preg_replace("/[\/_|+ -]+/", '-', $name);

		return $name;

	}


}
